


import scala.language.experimental.macros
import collection.mutable.Stack
import collection.mutable.ListBuffer
import scala.reflect.macros.Context

//import scala.language.experimental.macros

object MacroImpl {

  def println(c: Context)(format: c.Expr[String], params: c.Expr[Any]*): c.Expr[Unit] = {
    //Import the context universe.... this bit is absolutely crucial.
    //Wierd how the import is created from a parameter, I didn't know it was possible to use
    //imports in this fashion, it could be used as a context in the same manner as Spring or EJB.
    import c.universe._


    //    println(format.toString())
    //    println(params.toSeq)
    //
    //    println("dsafdsaafds")

    //println cause the intellij build to fail, I want to be able to debug object graphs etc,
    //and I don't know how well the debugger will handle this class so resorting to debug statements
    //might be a way to communicate state.
    c.warning(c.enclosingPosition, "a warning!")
    c.warning(c.enclosingPosition, "c.mirror.RootPackage.toString" +     c.mirror.RootPackage.toString)


    //
    //    println(format.toString())
    //    println(params.toSeq)


    val Literal(Constant(s_format: String)) = format.tree



    //    println(s_format)

    val evals = ListBuffer[ValDef]()

    // Real dark magic goes on here in the precompute method...
    def precompute(value: Tree, tpe: Type): Ident = {
      val freshName = newTermName(c.fresh("eval$"))
      evals += ValDef(Modifiers(), freshName, TypeTree(tpe), value)
      Ident(freshName)
    }
    val paramsStack = Stack[Tree]((params map (_.tree)): _*)
    val refs = s_format.split("(?<=%[\\w%])|(?=%[\\w%])") map {
      case "%d" => precompute(paramsStack.pop, typeOf[Int])
      case "%s" => precompute(paramsStack.pop, typeOf[String])
      case "%%" => Literal(Constant("%"))
      case part => Literal(Constant(part))
    }

    //    println(refs)
    val stats = evals ++ refs.map(ref => reify(print(c.Expr[Any](ref).splice)).tree)
    //val stats = evals ++ refs.map(ref => reify(print("dawg")).tree)

    val expr = c.Expr[Unit](Block(stats.toList, Literal(Constant(()))))
    //println(expr)
    //println(expr)
    expr
  }

}

