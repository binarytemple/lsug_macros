#!/bin/zsh

setopt extendedglob 

# Ok, so I had to create this script as the println inside the macro definition causes idea to think the build has failed.

scalac -d ./out/production/macros ./macros/src/**/*.scala

scalac -classpath ./out/production/macros -d ./out/production/example ./example/src/**/*.scala
