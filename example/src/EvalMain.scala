/**
 * An example of runtime evaluation.
 */
object EvalMain {
  def main(args: Array[String]) = {
    import scala.reflect.runtime._
    val cm = universe.runtimeMirror(getClass.getClassLoader)
    import scala.tools.reflect.ToolBox
    val tb = cm.mkToolBox()
    val parse = tb.parse( """println("1 + 1")""")
    tb.eval(parse)
    ()
  }
}
