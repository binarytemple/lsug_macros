//import scala.language.experimental.macros

object HelloMacrosWorld {

  def main(a:Array[String]) = {
    import MacroWrapper._
    println("The next output will be from a macro")
    makeIt("blah %s", "padddads")
  }
}
